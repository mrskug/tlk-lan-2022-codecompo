module Main (main) where

import           Criterion.Main    ( defaultMain, bench, bgroup, nf )
import qualified Data.Map.Internal as Map
import           Data.Maybe        ( fromJust, mapMaybe )
import           Text.Printf       ( printf )
import           Lib               ( parseInput, checkRoom )


main :: IO ()
main = do
    i <- readFile "input.txt"
    let results = mapMaybe checkRoom $ parseInput i
        sidSum  = sum $ map snd results
        nps     = fromJust $ Map.lookup "storage object northpole" $ Map.fromList results
        
    printf "\nWhat is the sum of the sector IDs of the real rooms?\nAnswer: %d\n" sidSum
    printf "\nWhat is the sector ID of the room where North Pole objects are stored?\nAnswer: %d\n" nps
    printf "\nRun with \"--output benchmark.html\" for more stats.\n\n "
    defaultMain [
        bgroup "TLK LAN codecompo" 
            [ bench "Sum of sector IDs" $ nf (sum . map snd . mapMaybe checkRoom . parseInput) i
            , bench "North pole object storage" $ nf (fromJust . Map.lookup "storage object northpole") (Map.fromList results)
            ]   
        ]