module Lib
    ( checkRoom
    , parseInput
    ) where

import           Data.Function     (on)
import qualified Data.List         as L
import qualified Data.List.Split   as L
import qualified Data.Map.Internal as Map

kcodes :: Map.Map Char Int
kcodes = Map.fromList $ L.zip ['a'..'z'] [(0::Int)..25]

alphas :: [Char]
alphas = ['a'..'z'] ++ ['a'..'z']

parseInput :: String -> [[String]]
parseInput = map (reverse . L.splitOn "-") . lines

checkRoom :: [String] -> Maybe (String, Int)
checkRoom (c:xs) = do
    let p = parseEnc $ L.concat xs
        csum = parseChecksum c
    comp (take (L.length $ snd csum) (map fst p)) csum xs
    where
        comp x y msg
            | x == snd y = Just (decodeMsg (fst y) msg, fst y)
            | otherwise = Nothing
checkRoom _ = Nothing

decodeMsg :: Int -> [[Char]] -> String
decodeMsg sid msg = do
    L.unwords $ map (map (decode sid)) msg
    where
        decode :: Int -> Char -> Char
        decode x c =
            case Map.lookup c kcodes of
                Just i -> alphas !! (i + mod x 26)
                _      -> 'Z'

parseEnc :: String -> [(Char, Int)]
parseEnc s = do
    let groups  = L.group $ L.sort s
        lengths = map L.length groups
    L.sortBy (flip compare `on` snd) $ L.zip (map head groups) lengths

parseChecksum :: String -> (Int,String)
parseChecksum = parser . L.splitOn "["
    where
        parser [sid,cs] = (read sid ::Int, take (L.length cs - 1) cs)
        parser _ = (0, [])